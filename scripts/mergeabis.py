#!/usr/bin/env python3

import sys
import json

merged = []
for a in sys.argv[1:]:
    f = open(a, 'r')
    j = json.load(f)
    f.close()
    merged += j

print(json.dumps(merged))
