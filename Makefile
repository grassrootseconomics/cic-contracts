# Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
# SPDX-License-Identifier:	GPL-3.0-or-later
# File-version: 5

INPUTS = $(wildcard solidity/*.sol)
OUTPUTS_JSON = $(patsubst %.sol, %.json, $(INPUTS))
OUTPUTS_INTERFACE = $(patsubst %.sol, %.interface, $(INPUTS))
OUTPUTS = $(OUTPUTS_JSON) $(OUTPUTS_INTERFACE)
PREFIX = /usr/local/share/cic/solidity/abi

#%.abi.json: $(wildcard *.sol)
#	install -vDm0644 $@ $(PREFIX)/$@

.SUFFIXES: .sol .json .interface

.sol.json:
	solc $(basename $@).sol --abi | awk 'NR>3' > $@

.sol.interface:
	bash to_interface.sh $(basename $@).sol > $@

all: $(OUTPUTS)
	echo $(OUTPUTS)

install: $(OUTPUTS)
	install -vDm0644 -t $(PREFIX) $?

clean:
	rm -vf solidity/*.json
	rm -vf solidity/*.interface

.PHONY: clean install
