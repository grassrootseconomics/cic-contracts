pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 5

interface Faucet {
	event FaucetUsed(address indexed _recipient, address indexed _token, uint256 _value);
	event FaucetFail(address indexed _recipient, address indexed _token, uint256 _value);
	event FaucetAmountChange(uint256 _value);

	// Address of token the faucet represents
	function token() external returns (address);
	// Amount of tokens the faucet gives out
	function tokenAmount() external returns (uint256);
	// Set the amount of tokens that the faucet gives out
	function setAmount(uint256 _amount) external returns (bool);
	// Give tokens to the given recipient
	function giveTo(address _recipient) external returns (bool);
	// Number of blocks that must be mined until faucet can be used for the same address
	// max uint (-1) can be used to indicate that faucet may not be reused
	function cooldown(address _recipient) external returns (uint256);
}
