pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 1

interface Minter {
	event Mint(address indexed _minter, address indexed _beneficiary, uint256 value);

	// Mint the specified value of tokens to the specified recipient
	function mintTo(address _beneficiary, uint256 value) external returns (bool);
}
