pragma solidity ^0.8.0;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 2

// This is a representation of an officially approved Ethereum Improvement Proposal, written by Nick Mudge <nick@perfectabstractions.com> and Dan Finlay <dan@danfinlay.com> and released under the CC0 license.
// The proposal source used as reference was a Markdown file with the following digests:
// - sha256:45e14ac315e380b5372a5ffc6783ab11d7eafb7fa5a123e0b8e5fc8c6c527c4c
// - swarmhash:0cadf6a7122d2da20dbab0ef31c692b1b24cf49ae5c1c80f7ce6dbae8885ce01
interface ERC173 {
	event OwnershipTransferred(address indexed previousOwner, address indexed newOwner); // EIP173

	function owner() external view returns (address);
	function transferOwnership(address _newOwner) external view returns (bool);
}
