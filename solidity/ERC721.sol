pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 1

// This is a representation of an officially approved Ethereum Improvement Proposal, William Entriken (@fulldecent), Dieter Shirley dete@axiomzen.co, Jacob Evans jacob@dekz.net, Nastassia Sachs nastassia.sachs@protonmail.com 
// The proposal source used as reference was a Markdown file with the following digests:
// - sha256:f25b41ad1bff14ec57e8c247f2ecf372fd86245d08ba3cbd005c21a42744a7ca
// - swarmhash:cf43d96b21b0b017ae32fb2ac8df9268f13344d9fd20aa1d959453eaf379a51b
interface ERC721 {
	event Transfer(address indexed _from, address indexed _to, uint256 indexed _tokenId);
	event Approval(address indexed _owner, address indexed _approved, uint256 indexed _tokenId);
	event ApprovalForAll(address indexed _owner, address indexed _operator, bool _approved);

	function balanceOf(address _owner) external view returns (uint256);
	function ownerOf(uint256 _tokenId) external view returns (address);
	function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes memory data) external payable;
	function transferFrom(address _from, address _to, uint256 _tokenId) external payable;
	function approve(address _approved, uint256 _tokenId) external payable;
	function setApprovalForAll(address _operator, bool _approved) external;
	function getApproved(uint256 _tokenId) external view returns (address);
	function isApprovedForAll(address _owner, address _operator) external view returns (bool);
}
