pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 1

// This is a representation of an officially approved Ethereum Improvement Proposal, written by Fabian Vogelsteller fabian@ethereum.org, Vitalik Buterin vitalik.buterin@ethereum.org. It was released under the CC0 license.
// The proposal source used as reference was a Markdown file with the following digests:
// - sha256:d120f321f2bef19596a401fba86d13352693ccd39645c2bbc84ffb3ed551388f
// - swarmhash:598f13429ec42f50d7a6576ab20feffdaf2135a90805678d24e4ea297bf0e639
interface ERC20 {
	event Transfer(address indexed _from, address indexed _to, uint256 _value);
	event TransferFrom(address indexed _from, address indexed _to, address indexed _spender, uint256 _value);
	event Approval(address indexed _owner, address indexed _spender, uint256 _value);

	function transfer(address _to, uint256 _value) external returns (bool);
	function transferFrom(address _from, address _to, uint256 _value) external returns (bool);
	function approve(address _spender, uint256 _value) external returns (bool);
	function name() external view returns (string memory);
	function symbol() external view returns (string memory);
	function decimals() external view returns (uint256);
	function totalSupply() external view returns (uint256);
	function allowance(address _owner, address _spender) external view returns(uint256);
	function balanceOf(address _holder) external view returns (uint256);
}
