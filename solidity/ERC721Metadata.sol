pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 1

// This is a representation of an officially approved Ethereum Improvement Proposal, William Entriken (@fulldecent), Dieter Shirley dete@axiomzen.co, Jacob Evans jacob@dekz.net, Nastassia Sachs nastassia.sachs@protonmail.com 
// The proposal source used as reference was a Markdown file with the following digests:
// - sha256:f25b41ad1bff14ec57e8c247f2ecf372fd86245d08ba3cbd005c21a42744a7ca
// - swarmhash:cf43d96b21b0b017ae32fb2ac8df9268f13344d9fd20aa1d959453eaf379a51b
interface ERC721Metadata {
	function name() external view returns (string memory _name);
	function symbol() external view returns (string memory _symbol);
	function tokenURI(uint256 _tokenId) external view returns (string memory);
}
