pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 2

interface Declarator {
	event DeclarationAdded(address _declarator, address _subject, bytes32 _proof);
	
	// Get all declarations for a subject signed by a declarator
	function declaration(address _declarator, address _subject) external view returns ( bytes32[] memory );
	// Get number of declarations the declarator has ever signed
	function declarationCount(address _declarator) external view returns ( uint256 );
	// Get the subject of a declarator's declarations at the specific index
	function declarationAddressAt(address _declarator, uint256 _idx) external view returns ( address );
	// Add a declaration for the subject
	function addDeclaration(address _subject, bytes32 _proof) external returns ( bool );
	// Get the declarator that signed a declaration at the specificed index for a subject
	function declaratorAddressAt(address _subject, uint256 _idx) external view returns ( address );
	// Get the number of declarators that have signed for a subject
	function declaratorCount(address _subject) external view returns ( uint256 );
}
