pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 3

interface AddressIndex {
	event AddressAdded(address indexed addedAccount, uint256 indexed accountIndex); // AccountsIndex

	// Return number of entries in index
	function entryCount() external view returns (uint256);
	// Return entry at the spceificed index
	function entry(uint256) external view returns (address);
	// Add an entry to the index
	function add(address _account) external returns (bool);
	// Verify that the entry exists in the index
	function have(address _account) external view returns (bool);
}
