pragma solidity ^0.8.0;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 2

// This is a representation of an officially approved Ethereum Improvement Proposal, written by Christian Reitwießner chris@ethereum.org, Nick Johnson nick@ethereum.org, Fabian Vogelsteller fabian@lukso.network, Jordi Baylina jordi@baylina.cat, Konrad Feldmeier konrad.feldmeier@brainbot.com and William Entriken github.com@phor.net. It was released under the CC0 license.
// The proposal source used as reference was a Markdown file with the following digests:
// - sha256:2fc8534206e1e5b7abbb9db21fa5b83d39c51b2dabad441a356b7d18320bfc51
// - swarmhash:a1a2860be878a818a9207503fbbe49cdbd86c3e02a4fdac0a4faafa78f6bdd80
interface ERC165 {
	function supportsInterface(bytes4 _sum) external pure returns (bool);
}
