pragma solidity >=0.6.12;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 1

interface Writer {
	function addWriter(address _writer) external returns (bool);
	function deleteWriter(address _writer) external returns (bool);
}
