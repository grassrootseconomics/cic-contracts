#!/bin/bash

d=`mktemp -d`
b=`basename $1`
s=${b%%.*}
python3 scripts/reduce_to_methods.py $(realpath $1) > $d/$b
python3 scripts/calculate_eip165.py $d/$b
